#include "vespaBooleanOperationCLP.h"

#include <vtkCGALBooleanOperation.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

static int CGALBooleanOperationFromString(const std::string& str)
{
  if (str == "difference")
  {
    return vtkCGALBooleanOperation::DIFFERENCE;
  }
  else if (str == "intersection")
  {
    return vtkCGALBooleanOperation::INTERSECTION;
  }
  else if (str == "union")
  {
    return vtkCGALBooleanOperation::UNION;
  }

  return -1;
}

extern "C" 
{

int ModuleEntryPoint(int argc, char* argv[])
{
  PARSE_ARGS

  vtkNew<vtkXMLPolyDataReader> firstPolyReader;
  firstPolyReader->SetFileName(firstPoly.c_str());

  vtkNew<vtkXMLPolyDataReader> secondPolyReader;
  secondPolyReader->SetFileName(secondPoly.c_str());

  vtkNew<vtkCGALBooleanOperation> boolFilter;
  boolFilter->SetOperationType(CGALBooleanOperationFromString(operation));
  boolFilter->SetInputConnection(firstPolyReader->GetOutputPort());
  boolFilter->SetSourceConnection(secondPolyReader->GetOutputPort());
 
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetFileName(output.c_str());
  writer->SetInputConnection(boolFilter->GetOutputPort());
 
  return writer->Write() ? 0 : 1;
}

}