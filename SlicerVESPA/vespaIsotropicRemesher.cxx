#include "vespaIsotropicRemesherCLP.h"

#include <vtkCGALIsotropicRemesher.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

extern "C" 
{

int ModuleEntryPoint(int argc, char* argv[])
{
  PARSE_ARGS

  vtkNew<vtkXMLPolyDataReader> polyReader;
  polyReader->SetFileName(poly.c_str());

  vtkNew<vtkCGALIsotropicRemesher> isotropicRemesher;
  isotropicRemesher->SetInputConnection(polyReader->GetOutputPort());
  isotropicRemesher->SetProtectAngle(protectAngle);
  isotropicRemesher->SetTargetLength(targetLength);
  isotropicRemesher->SetNumberOfIterations(numberOfIterations);
 
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetFileName(output.c_str());
  writer->SetInputConnection(isotropicRemesher->GetOutputPort());
 
  return writer->Write() ? 0 : 1;
}

}